cd "$(mktemp -d)"
cat > update_winetricks <<_EOF_SCRIPT
#!/bin/sh

cd "\$(mktemp -d)"
wget https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks
wget https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks.bash-completion
chmod +x winetricks
su root sh -c 'mv winetricks /usr/bin ; mv winetricks.bash-completion /usr/share/bash-completion/completions/winetricks'
_EOF_SCRIPT

chmod +x update_winetricks
su root sh -c 'mv update_winetricks /usr/bin/'
