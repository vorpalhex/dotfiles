local wezterm = require 'wezterm'
return {
    font = wezterm.font("Fira Code"),
    keys = {
    -- Turn off the default CMD-m Hide action, allowing CMD-m to
    -- be potentially recognized and handled by the tab
        {key="d", mods="CTRL|SHIFT", action=wezterm.action{SplitVertical={domain="CurrentPaneDomain"}}},
        {key="d", mods="CTRL", action=wezterm.action{SplitHorizontal={domain="CurrentPaneDomain"}}}
    },
    unix_domains = {
        {
          name = "unix",
          connect_automatically = true,
        }
    },
    ssh_domains = {
        {
          -- This name identifies the domain
          name = "stronghold",
          -- The address to connect to
          remote_address = "stronghold.quirkyraven.space",
          -- The username to use on the remote host
          username = "kyros",
        }
    }
}

