#!/bin/bash

# init
echo "### Vorpalhex's dotfile installer ###"

echo "### Inflating... ###"
git submodule init
git submodule update


if [[ $(uname) =~ "Darwin" ]]; then
  echo "### MacOS Stuff ###"
  # install brew
  if ! type 'brew' > /dev/null ; then 
    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
  fi
  # brew install stuff
  brew install curl exiftool ffmpeg jq httpie mtr stow wget youtube-dl
  # brew casks
  brew install --cask firefox vscodium 
  # 
  echo "...done"
fi

echo "### Basics... ###"
# check to see if we have zsh here.. probably
echo "see if we need to install oh-my-zsh"
if [[ $ZSH ]]; then
  echo "...nope, already got oh-my-zsh"
else
  sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
fi
echo "zsh prefs..."
stow zsh
[[ ! -a $HOME/.local.sh ]] && touch $HOME/.local.sh
echo "vim prefs..."
stow vim
echo "git prefs..."
stow git
echo "common.."
stow common
echo "powerline fonts..."
./deps/fonts/install.sh
echo "dev stuff"
stow dev
echo "### Done ###"

# Done
echo "Thanks! Have a nice day!"
