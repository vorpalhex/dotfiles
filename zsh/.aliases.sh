# zsh
alias resrc='source ~/.zshrc'

# Ensure agent is running
ssh-add -l &>/dev/null
if [ $? \== 2 ]; then
    # Could not open a connection to your authentication agent.

    # Load stored agent connection info.
    test -r ~/.ssh-agent && \
        eval "$(<~/.ssh-agent)" >/dev/null

    ssh-add -l &>/dev/null
    if [ $? \== 2 ]; then
        # Start agent and store agent connection info.
        (umask 066; ssh-agent > ~/.ssh-agent)
        eval "$(<~/.ssh-agent)" >/dev/null
    fi
fi

# Load identities
ssh-add -l &>/dev/null
if [ "$?" \== 1 ]; then
    # The agent has no identities.
    # Time to add one.
    ssh-add -t 24h
fi

# git
alias gcm='git commit -m'
alias gap='git add -p .'
alias ga='git add .'
alias gacm='git add -p . && git commit -m'
alias gp='git push'
alias gpl='git pull'
alias task='screen -d -m'

# ssh
sshm() {
  /usr/bin/ssh -t "$@" "tmux attach -t ssh || tmux new -t ssh"
}
alias dc='tmux detach -P'

# misc
alias fix-locale="perl -pi -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/g' /etc/locale.gen && locale-gen en_US.UTF-8 && update-locale en_US.UTF-8"
#alias notes='code ~/KB'

# vscodium
alias code='codium'

# mac specific
if [[ `uname` == "Darwin" ]]; then
  alias flushdns="sudo killall -HUP mDNSResponder"
fi

# prefs
#alias notes='code ~/KB'
#alias blog='code ~/Projects/vorpalhex.gitlab.io'

# server bits
alias smartctlseagate='smartctl -v 1,raw48:54'
alias mirror='wget --mirror --no-parent'
alias spider='wget --spider'

# sharing
function share() {
  if [ ! -f $1 ]; then
    echo "File not found. Invoke 'share ../some/file {optional rename}'"
    exit 1
  fi
  local remote="https://share.vorpalhex.dev"
  local fn=$1:t
  if [ "$#" -eq 2 ]; then
    fn=$2
  fi
	
  local guid=$(uuidgen -r)
  rclone copyto --progress "$1" "share:/$guid/$fn"
  local dest=$(omz_urlencode "$fn")
  echo "---"
  echo "$remote/$guid/$dest"
}

# kube
alias k8ns='kubectl config set-context --current --namespace'
alias pods='kubectl get pods'

# vr video
scale24k(){
ffmpeg -i $1 -vf "scale=w=-2:h='min(2400,ih)':sws_flags=spline+accurate_rnd:in_range=tv:out_range=tv" -c:v libx264 -colorspace bt709 -color_trc bt709 -color_primaries bt709 -color_range tv -crf 17 -preset slow -tune film -x264-params mvrange=511 -pix_fmt yuv420p -c:a copy -movflags +faststart $2
}


# emulation
alias convertccds='for f in **/*.ccd; do ccd2cue $f; done'
alias makechd='for f in **/*.cue; do name=${f%.cue}; chdman createcd -i "$name.cue" -o "$name.chd" --force; done'
