export DEFAULT_USER=kyros
PATH="$HOME/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
source /etc/profile
PATH=$PATH:~/.npm_modules/bin:~/.cargo/bin:~/.npm_global/bin:/usr/local/sbin:/snap/bin:~/.local/bin:~/.pyenv/bin

export PIPENV_VENV_IN_PROJECT=1
export PS1=%{%f%b%k%}$(build_prompt)