-- watch config and autoreload
local myWatcher = hs.pathwatcher.new(os.getenv("HOME") .. "/.hammerspoon/", hs.reload):start()

-- bypass paste blockers
hs.hotkey.bind({"cmd", "alt"}, "V", function() hs.eventtap.keyStrokes(hs.pasteboard.getContents()) end)

-- button to toggle office lamp
function setLamp(state)
  command = "python2 ~/.hammerspoon/tplink_smartplug.py -c "..state.." -t 192.168.0.98"
  print(command)
  res = hs.execute(command, true)
  print(res)
end

lampState = "off"

function toggleLamp()
  if lampState == "on" then
    lampState = "off"
  else
    lampState = "on"
  end
  setLamp(lampState)
  setLampDisplay(lampState)
end

lamp = hs.menubar.new(true)

function setLampDisplay(state)
  if state then
      lamp:setIcon("./icons/lightbulb-on.png")
  else
      lamp:setIcon("./icons/lightbulb-off.png")
  end
end

if lamp then
    lamp:setClickCallback(toggleLamp)
    setLampDisplay(lampState)
end

-- caffeine replacement
caffeine = hs.menubar.new()
function setCaffeineDisplay(state)
    if state then
        caffeine:setIcon("./icons/coffee-full.png")
    else
        caffeine:setIcon("./icons/coffee-empty.png")
    end
end

function caffeineClicked()
    setCaffeineDisplay(hs.caffeinate.toggle("displayIdle"))
end

if caffeine then
    caffeine:setClickCallback(caffeineClicked)
    setCaffeineDisplay(hs.caffeinate.get("displayIdle"))
end
