#!/bin/bash

echo "Setting up Mac OS stuff..."

if [[ ! type brew >/dev/null 2>&1 ]]
then
  read -p "Do you want me to install homebrew? " -n 1 -r
  echo
  if [[ $REPLY =~ ^[Yy]$ ]]
  then
    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
  fi
fi

if [[ type brew >/dev/null 2>&1 ]]
then
  read -p "Do you want me to install common tools like wget and jq? " -n 1 -r
  echo
  if [[ $REPLY =~ ^[Yy]$ ]]
  then
    brew install wget
    brew install jq
    brew install python3
    brew install pip3
    brew cask install firefox
    brew cask install iterm2
    brew cask install caffeine
  fi
fi

mkdir -p $HOME/.hammerspoon
cp ./*.lua $HOME/.hammerspoon
cp -R ./icons $HOME/.hammerspoon
cp ./tplink_smartplug.py $HOME/.hammerspoon
# install iterm2, caffeine, etc
