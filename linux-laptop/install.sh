sudo apt update -y
sudo apt install -y software-properties-common zsh

# repos
## pritunl
echo "deb https://repo.pritunl.com/stable/apt focal main" | sudo tee /etc/apt/sources.list.d/pritunl.list
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com --recv 7568D9BB55FF9E5287D586017AE645C0CF8E292A
## dell xps shit
echo "deb http://dell.archive.canonical.com/updates/ focal-oem public" | sudo tee /etc/apt/source.list.d/focal-dell.list
echo "deb http://dell.archive.canonical.com/updates/ focal-somerville public" | sudo tee /etc/apt/source.list.d/focal-dell.list
echo "deb http://dell.archive.canonical.com/updates/ http://dell.archive.canonical.com/updates/ focal-somerville-melisa public" | sudo tee /etc/apt/source.list.d/focal-dell.list
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys F9FDA6BED73CDC22
## kopia backup
echo "deb http://packages.kopia.io/apt/ stable main" | sudo tee /etc/apt/sources.list.d/kopia.list
curl -s https://kopia.io/signing-key | sudo apt-key add -

# utilities
sudo apt install -y xclip xsel powertop mtr

# power
sudo apt install -y powertop tlp-config

# xps and fingerprint
sudo apt install -y oem-somerville-meta oem-somerville-melisa-meta
sudo apt install -y fprintd libfprint0 libfprint-2-tod1-goodix libpam-fprintd

# apps

## ghostwriter
sudo add-apt-repository ppa:wereturtle/ppa
sudo apt install -y ghostwriter
sudo apt install -y steam-launcher libc6-i386 libgl1-mesa-glx libgl1-mesa-dri:i386 libgl1:i386 libsdl2-mixer-2.0-0/focal libsdl2-ttf-2.0-0/focal libsdl2-image-2.0-0/focal
## yubikey
sudo apt-add-repository ppa:yubico/stable
sudo apt install -y yubikey-manager
## snap
sudo apt install -y snapd
## docker
sudo apt install -y docker docker-compose
## gnome shit
sudo apt install -y gparted gnome-disk-utility gdebi gnome-sushi dconf-editor
## wine
sudo apt install -y wine64 winetricks wine
## kopia
sudo apt update
sudo apt install kopia kopia-ui

## snap
snap install codium mailspring snap-store spotify vlc