set number
set showbreak=+++
set textwidth=100
set showmatch
set visualbell

set hlsearch
set smartcase
set ignorecase
set incsearch

set autoindent
set shiftwidth=4
set smartindent
set smarttab
set softtabstop=4

set ruler

set undolevels=1000
set backspace=indent,eol,start

set nocompatible              " be iMproved, required
filetype off                  " required
execute pathogen#infect('~/.vim-bundle/{}')
syntax on
filetype plugin indent on

set laststatus=2
