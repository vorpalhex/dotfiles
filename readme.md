# vorpals dotfiles

## ubuntu-ish setup (pop-os)

1. Copy over .ssh and .config dirs out of band
2. Install zsh
3. Clone this repo to ~/.dotfiles and run install.sh
4. Go through prompts
5. Run `tlp` and enable all options and set wake on lan on all usb-c ports for docking
6. Set hostname to something sensible
7. Make sure dropbox, 1password and firefox are functioning as expected